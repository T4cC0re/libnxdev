# libNXdev

## What is this?

This is a docker container containing the open-souce Nintendo Switch SDK [libnx](https://github.com/switchbrew/libnx), [hacBrewPack](https://github.com/The-4n/hacBrewPack) and various tools from [devkitPro](https://github.com/devkitPro) bundled into ArchLinux.

 ## How to use?

The image has `/app` set as it's working directory, so that is what is easiest to use. But if you are using e.g. GitLab-CI you also don't need to worry. It just works, too! Just use `registry.gitlab.com/t4cc0re/libnxdev` as the image

To run `make` in your current homebrew source folder:
```
docker run -v $(pwd):/app registry.gitlab.com/t4cc0re/libnxdev make
```

Replace `make` with whichever tool you need. E.g.
```
docker run -v $(pwd):/app registry.gitlab.com/t4cc0re/libnxdev hacbrewpack
```

## What does it contain?

It contains these tools (in `$PATH`):
 - [hacBrewPack](https://github.com/The-4n/hacBrewPack)

In addition to that it also contains the following packages sourced from both ArchLinux repos and devkitPro repos:

 - autoconf
 - automake
 - binutils
 - bison
 - devkitA64
 - devkit-env
 - fakeroot
 - file
 - findutils
 - flex
 - gawk
 - gcc
 - general-tools
 - gettext
 - grep
 - groff
 - gzip
 - libnx
 - libtool
 - m4
 - make
 - pacman
 - patch
 - pkgconf
 - sed
 - sudo
 - switch-bulletphysics
 - switch-bzip2
 - switch-curl
 - switch-examples
 - switch-ffmpeg
 - switch-flac
 - switch-libass
 - switch-libconfig
 - switch-libfribidi
 - switch-libjpeg-turbo
 - switch-libjson-c
 - switch-libmikmod
 - switch-libopus
 - switch-libsodium
 - switch-libtheora
 - switch-libvorbis
 - switch-libvorbisidec
 - switch-libxml2
 - switch-mbedtls
 - switch-ode
 - switch-opusfile
 - switch-sdl2
 - switch-sdl2_gfx
 - switch-sdl2_image
 - switch-sdl2_mixer
 - switch-sdl2_net
 - switch-sdl2_ttf
 - switch-tools
 - switch-zziplib
 - systemd
 - texinfo
 - util-linux
 - which

 ## License
 
 This image souce (Dockerfile + config) itself is licensed under WTFPL, but anything contained in it (see packages and tools above) retains the license of the appropriate source.
 
 ```
            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
                    Version 2, December 2004

 Copyright (C) 2019 Hendrik Meyer

 Everyone is permitted to copy and distribute verbatim or modified
 copies of this license document, and changing it is allowed as long
 as the name is changed.

            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION

  0. You just DO WHAT THE FUCK YOU WANT TO.

 ```
