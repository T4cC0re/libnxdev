FROM archlinux/base

ARG	packages="switch-dev switch-tools switch-portlibs base-devel git"
ENV     DEVKITPRO=/opt/devkitpro
ENV     DEVKITARM=/opt/devkitpro/devkitARM
ENV     DEVKITPPC=/opt/devkitpro/devkitPPC
ENV	PATH="/opt/devkitpro/tools/bin:${PATH}"

WORKDIR	/app

ADD	pacman.conf	/etc/pacman.conf

RUN 	pacman-key --init && \
	pacman-key --recv F7FD5492264BB9D0 && \
	pacman-key --lsign F7FD5492264BB9D0 && \
	pacman --noconfirm -U https://downloads.devkitpro.org/devkitpro-keyring-r1.787e015-2-any.pkg.tar.xz && \
	pacman --noconfirm -Syu ${packages} && \
	rm -rf /var/cache/pacman && \
	git clone https://github.com/The-4n/hacBrewPack.git /opt/hacBrewPack && \
	cd /opt/hacBrewPack && \
	cp config.mk.template config.mk && \
	make && \
	cp hacbrewpack /opt/devkitpro/tools/bin/ && \
	cd /app && \
	rm -r /opt/hacBrewPack
